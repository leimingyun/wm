## 主要目的
向低代码靠拢，只需要增删查改调方法排积木，就可以实现项目的快速开发及上线。  
给整个开发制定一套固定规则，以期望实现新人入职不用带、极速上手参与研发、老员工离职好交接等。  
 
## 零基础应届生入门学习步骤
这里是指计算机系毕业的应届生，学过java基础，在没接触过任何框架的情况下、学习成绩中等偏下的情况下，依靠本入门文档的学习步骤，两天快速熟练入门，第三天即可参与配合项目经理打下手，一周即可进入开发状态

1. 准备好Eclipse（如果没安装需自行下载）
1. [git中拉取项目](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390) - git的提交与拉取的使用
1. [hello word，代码入门](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3601196&doc_id=1101390)
1. [配置mysql数据库](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3634642&doc_id=1101390)
1. [实体类创建示例，入门及规范](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3368778&doc_id=1101390)
1. [数据表的基本增删查改的使用](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3601212&doc_id=1101390)
1. [练习列表-增-删-查-改的DEMO](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4020870&doc_id=1101390)
1. [jsp动态标签](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4176590&doc_id=1101390)
1. [编辑页面进阶-下拉选择框的使用](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4169447&doc_id=1101390)
1. [数据表分表及增删查改实现](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4172714&doc_id=1101390)
1. [图片上传](?doc_id=1101390&sort_id=4175169)
1. [jsp前端页面常用js方法](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3276840&doc_id=1101390)
1. [管理后台菜单及权限入门](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4158521&doc_id=1101390)
1. [json接口开发，demo示例入门](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4160722&doc_id=1101390)

...  
[更多参阅 wm.zvo.cn](http://wm.zvo.cn)

## 来源及简介
由开源CMS领域全国第二的 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket) ，从中抽取出一些最基本的方法接口，如文件上传、短信验证码验证、数据库操作、缓存、海量日志存储、多语言、权限……做成了一个个的积木，实际做项目时，只需要排积木，一句代码通过开放的方法及接口调用，即可快速完成相应功能，而你无需关注其底层具体如何实现。


## 现有的系统管理后台功能模块  
1. 用户管理系统  
1. 权限系统  
1. 日志系统  
1. 系统变量（system数据表配置的全局参数）  
1. 短信验证（短信验证码发送管理）  
  
这些功能是已经有的，可以直接登录管理后台即可看到。  
登录地址 /login.do ，账号密码都是admin

## 快速开发的方法接口及功能示例
1. 数据库操作，增删查改、执行sql语句等（MySQL、Sqlite、...）
1. 文件、附件上传 （上传到服务器本地、华为云OBS、阿里云OSS、...）
1. 前端页面常用js方法的使用(ajax请求、信息提醒、加载中、...)
1. 缓存，key-value缓存（redis、javamap自身缓存）
1. 海量日志存储及全文搜索引擎（ElasticSearch、阿里云日志服务等）
1. 用户相关，登录、注册、冻结、改密等
1. 短信发送、短信验证码验证
1. 多语言支持、多国语言包
1. 图片验证码的使用及验证
1. application.properties 配置文件的读取使用
1. MQ消息推送分发
1. Session使用及扩展
1. 安全效验及过滤（SQL注入、XSS攻击）
1. Spring 所管理 Bean 的快速获取
1. 前端jsp页面的通用模块(头部、分页、尾部等)
1. Shiro权限及管理后台菜单
1. wm 的系统配置参数获取使用（system表的配置数据）
1. wm 的重写，覆盖
1. wm 的插件开发

## 开发环境
Java 1.8  
Maven 3

## Java后端开发框架
SpringBoot 2.0
Shiro(已集成，不熟悉可以选择不用)
rabbitmq(已集成，默认不启用)
elasticsearch(已集成，默认不启用)
redis(已集成，默认不启用)
云存储(OBS、OSS等，已集成，默认不启用)

## 前端开发框架
msg.js （开发文档： [https://gitee.com/mail_osc/msg](https://gitee.com/mail_osc/msg) ）
Layui 2 

