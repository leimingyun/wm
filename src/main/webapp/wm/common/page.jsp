<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type='text/javascript' src='/module/page/page.js?v=<%=Global.VERSION %>'></script>
<script>
page.upNextPageNumber = 2;	//上几页、下几页，显示的数。如这里是2，则向上会显示2页
</script>
<!-- 这是日志列表页面底部的分页显示 -->
<style>
	#page{
		width:100%;
		padding-top:15px;
		padding-bottom: 20px;
	}
	#page ul{
		width: 100%;
		text-align: center;
	}
	#page ul li{
		display: inline-block;
		vertical-align: middle;
		border: 1px solid #e2e2e2;
		background-color: #fff;
		padding-right: 1px;
		padding-left: 1px;
	}
	#page ul .xnx3_page_currentPage, #page ul .xnx3_page_currentPage a{
		background-color: #009688;
		color:#fff;
	}
	#page ul li a{
	 	padding: 0 15px;
	 	height: 30px;
		line-height: 30px;
		background-color: #fff;
		color: #333;
	 }
</style>

<!-- id的值不能改 -->
<div id="page" style="display:none;">
	<ul>
		<!-- 判断当前页面是否是列表页第一页，若不是第一页，那会显示首页、上一页的按钮。 id的值不能改 -->
		<span id="firstPage">
			<li><a href="javascript:wm.list(1);">首页</a></li>
			<li><a href="javascript:wm.list('{upPageNumber}');">上一页</a></li>
		</span>
		
		<!-- 输出上几页的连接按钮。id的值不能改 --> 
		<span id="upList">
			<li><a href="{href}">{title}</a></li>
		</span>
		
		<!-- 当前页面，当前第几页 -->
		<li class="xnx3_page_currentPage"><a href="#">{currentPageNumber}</a></li>
		
		<!-- 输出下几页的连接按钮。 id的值不能改 --> 
		<span id="nextList">
			<li><a href="{href}">{title}</a></li>
		</span>
		
		<!-- 判断当前页面是否是列表页最后一页，若不是最后一页，那会显示下一页、尾页的按钮。 id的值不能改 -->
		<span id="lastPage">
			<li><a href="javascript:wm.list('{nextPageNumber}');">下一页</a></li>
			<li><a href="javascript:wm.list('{lastPageNumber}');">尾页</a></li>
		</span>
		
		<li style="margin-right:30px;border:0px; padding-top:5px;">共{allRecordNumber}条，{currentPageNumber}/{lastPageNumber}页</li>
	</ul>
</div>

<style>
	#xnx3_page{
		width:100%;
		padding-top:15px;
		padding-bottom: 20px;
	}
	#xnx3_page ul{
		width: 100%;
		text-align: center;
	}
	#xnx3_page ul li{
		display: inline-block;
		vertical-align: middle;
		border: 1px solid #e2e2e2;
		background-color: #fff;
		padding-right: 1px;
		padding-left: 1px;
	}
	#xnx3_page ul .xnx3_page_currentPage, #xnx3_page ul .xnx3_page_currentPage a{
		background-color: #009688;
		color:#fff;
	}
	#xnx3_page ul li a{
	 	padding: 0 15px;
	 	height: 30px;
		line-height: 30px;
		background-color: #fff;
		color: #333;
	 }
</style>
<div id="xnx3_page" style="display:none;">
	<ul>
		<!-- 判断当前页面是否是列表页第一页，若不是第一页，那会显示首页、上一页的按钮 -->
		<c:if test="${!page.currentFirstPage}">
			<li><a href="${page.firstPage }">首页</a></li>
			<li><a href="${page.upPage }">上一页</a></li>
		</c:if>
		
		<!-- 输出上几页的连接按钮 -->
		<c:forEach items="${page.upList}" var="a">
			<li><a href="${a.href }">${a.title }</a></li>
		</c:forEach>
		
		<!-- 当前页面，当前第几页 -->
		<li class="xnx3_page_currentPage"><a href="#">${page.currentPageNumber }</a></li>
		
		<!-- 输出下几页的连接按钮 -->
		<c:forEach items="${page.nextList}" var="a">
			<li><a href="${a.href }">${a.title }</a></li>
		</c:forEach>
		
		<!-- 判断当前页面是否是列表页最后一页，若不是最后一页，那会显示下一页、尾页的按钮 -->
		<c:if test="${!page.currentLastPage}">
			<li><a href="${page.nextPage }">下一页</a></li>
			<li><a href="${page.lastPage }">尾页</a></li>
		</c:if>
		
		<li style="margin-right:30px;border:0px; padding-top:5px;">共${page.allRecordNumber }条，${page.currentPageNumber }/${page.lastPageNumber }页</li>
	</ul>
</div>
<script>
var app;
var page_currentPageNumber = '${page.currentPageNumber }';
if(typeof(page_currentPageNumber) != 'undefined' && page_currentPageNumber.length > 0){
	//不是使用的ajax方式加载的
	try{
		document.getElementById('page').style.display='none';
	}catch(e){ console.log(e); }
	try{
		document.getElementById('xnx3_page').style.display='';
	}catch(e){ console.log(e); }
}else{
	//使用的ajax加载的，判断是否重写了list的js方法
	try{
		document.getElementById('xnx3_page').style.display='none';
	}catch(e){ console.log(e); }
	try{
		document.getElementById('page').style.display='';
	}catch(e){ console.log(e); }
	
	window.onload = function(){
		//当页面加载完成
		if(typeof(list) == 'undefined'){
			console.log('jsp中请重写js的list(pageNumber)方法。');
			//未重写list，那么实现list方法。 currentPage要查看的是第几页的数据，传入如 1
			window.list = function(currentPage){
				msg.info('jsp页面中请用js写一个list(1)的方法，来加载指定第几页的数据。');
			}
		}
	}
	
	
	//找出当前cache/js/ 引入的缓存js
	var scriptTags = document.getElementsByTagName('script');
	var scriptTagsList = new Array();
	for(var i = 0; i<scriptTags.length; i++){ 
		if(scriptTags[i].src != null && scriptTags[i].src != '' && scriptTags[i].src.indexOf('/cache/js/') > 0){
			var fileName = scriptTags[i].src.split("/cache/js/")[1];	//取得如 Person_color.js
			var fileName_removeSuffix = fileName.split(".")[0];			//得到如 Person_color
			var fileName_removeClassName = fileName_removeSuffix.replace(fileName_removeSuffix.split("_")[0]+'_', '');		//得到如 color_type
			scriptTagsList.push(fileName_removeClassName);
		} 
	}
	
	if(typeof(Vue) != 'undefined'){
		//使用的是wm1.23以后版本，引入了vue
		app = Vue.createApp({
			data() {
				return {
					list: []
				}
			},
			methods:{
				/** 
				 * 时间戳转化为年 月 日 时 分 秒 
				 * number: 传入时间戳 如 1587653254
				 * format：返回格式，如 'Y-M-D h:m:s'
				*/
				formatTime(number,format){
					return wm.formatTime(number,format);
				}
			}
		}).mount('.iw_table');
		//执行eval方法，传入字符串方法名
		wm.executeEval = function(functionName){
			return eval(functionName);
		}
		for(var i = 0; i<scriptTagsList.length; i++){
			//key变为驼峰方式，如 key:colorType ；   value不变依旧是带下划线的方式:color_type
			app.$data[wm.lineToHump(''+scriptTagsList[i])] = wm.executeEval(scriptTagsList[i]);
			//将非驼峰方式的也加进去，增加容错（writecode自动写出的是非驼峰的）
			app.$data[''+scriptTagsList[i]] = wm.executeEval(scriptTagsList[i]);
		}
		
		/**
		 * 分类列表
		 * @param currentPage 要查看第几页，如传入 1
		 * @param api list列表数据的api接口，传入如 /demo/person/list.json
		 * @param extendFunction 更多扩展，传入 function(){ console.log(app.result); } 如果不传入，则不会执行。默认不传入即可。
		 */
		wm.list = function(currentPage, api, extendFunction){
			if(typeof(api) != 'undefined' && api != null){
				wm.list_api = api;
			}
			if(typeof(wm.list_api) == 'undefined' || wm.list_api == null || wm.list_api.length < 1){
				wm.list_api = 'list.json';
			}
			var data = wm.getJsonObjectByForm($('.toubu_xnx3_search_form'));
			data.currentPage = currentPage;
			msg.loading('加载中');
			wm.post(wm.list_api ,data,function(data){
				msg.close();	//关闭“更改中”的等待提示
				checkLogin(data);	//判断是否登录
			
				//将json返回的赋予 vue的全局变量
				for(var key in data){
					app[key] = data[key];
				}
				
				//已登陆
				if(data.result == '1'){
					//分页,如果有分页数据传回，那么显示分页
					if(typeof(data.page) != 'undefined' && data.page != null){
						page.render(data.page);
					}else{
						//没有分页数据，那么不显示上一页下一页的
						document.getElementById('page').style.display = 'none';
					}
					
					if(typeof(extendFunction) != 'undefined'){
						extendFunction();
					}
				}else{
					msg.failure(data.info);
				}
			});
		}
	}
}

</script>