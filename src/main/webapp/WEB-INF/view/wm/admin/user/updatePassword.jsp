<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="会员资料信息"/>
</jsp:include>
<style>
	body {
		background: #FFF
	}
</style>

<form id="form" class="layui-form" action="" style="padding:20px; padding-top:35px; margin-bottom: 10px;">
	
	<div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">您的旧密码</label>
		<div class="layui-input-block">
			<input type="text" name="oldPassword" id="oldPassword" required autocomplete="off" placeholder="输入旧密码" class="layui-input" value="">
		</div>
	</div>
	
	<div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">新密码</label>
		<div class="layui-input-block">
			<input type="text" name="newPassword" id="newPassword" required autocomplete="off" placeholder="输入新密码" class="layui-input" value="">
		</div>
	</div>
	
	<div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">确认新密码</label>
		<div class="layui-input-block">
			<input type="text" name="reNewPassword" id="reNewPassword" required autocomplete="off" placeholder="再次输入新密码" class="layui-input" value="">
		</div>
	</div>
	
	
	<div class="layui-form-item">
		<div class="layui-input-block">
			<input type="button" class="layui-btn" style="width: 240px;" value="提交" onclick="updatePassword()"/>
		</div>
	</div>
</form>

<script type="text/javascript">
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

//修改密码
function updatePassword() {
	if ($("#oldPassword").val() == "") {
		msg.failure("请输入旧密码");
		return;
	}
	if ($("#newPassword").val() == "") {
		msg.failure("请输入新密码");
		return;
	}
	if ($("#reNewPassword").val() == "") {
		msg.failure("请再次输入新密码");
		return;
	}
	if ($("#newPassword").val() != $("#reNewPassword").val()) {
		msg.failure("两次输入的密码不匹配");
		return;
	}
	
	msg.loading("保存中");
	var d=$("form").serialize();
	$.post("updatePassword.json", d, function (result) {
		msg.close();
		var obj = JSON.parse(result);
		if(obj.result == '1'){
			parent.msg.success("操作成功");
			parent.layer.close(index);	//关闭当前窗口
			parent.location.reload();	//刷新父窗口列表
		}else if(obj.result == '0'){
			parent.msg.failure(obj.info);
		}else{
			parent.msg.failure(result);
		}
	}, "text");
	return false;
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include>