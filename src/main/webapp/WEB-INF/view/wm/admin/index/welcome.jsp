<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="欢迎使用"/>
</jsp:include>

<div style="padding-top:22%; width:100%; text-align:center; font-size:2.2rem">
	欢迎您，<span class="ignore">${user.username }</span>
</div>

<jsp:include page="/wm/common/foot.jsp"></jsp:include>
<style> /* 显示多语种切换 */ .translateSelectLanguage{ display:block; } </style>