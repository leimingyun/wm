<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</body>
<script type="text/javascript">
//form组件，开启select
layui.use(['form'], function(){
	var form = layui.form;
	//判断是否加载了JQuery，正常情况下，只要引入了head，自然也就引入了JQuery
	if(typeof(jQuery)!="undefined"){
		//页面上有select标签，才会进行绑定
		if(typeof($('select')[0])!="undefined"){
			//绑定iw的orderBy自动排序，当编辑方式发生变动改变
			form.on('select(selectOrderBy)', function (data) {
				var selObj = document.getElementById("selectOrderBy_xnx3_id");
				var xnx3_sel_index = selObj.selectedIndex;
				if(xnx3_sel_index != defaultShow_index){
					var url = selObj.options[xnx3_sel_index].value;
					
					if(typeof(wm.list) == 'undefined'){
						//jsp页面
						window.location.href = url;
					}else{
						//读写分离的
						
						//取 orderby的值
						// 使用正则表达式匹配出orderBy 参数及其值
						var orderByValue = '';
						let match = url.match(/orderBy=(\w+)/);
						if (match) {
							orderByValue = match[1];
						} else {
						  console.log("未找到orderBy 参数");
						}
						
						if(document.getElementsByClassName('toubu_xnx3_search_form').length == 0){
							msg.alert('开发提示<br/>请<a href="https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=8450750&doc_id=1101390">点此参考文档</a> 必须放到 from 里，不然点击后，这个orderBy 的参数会传不过去');
						}
						if(document.getElementsByClassName('toubu_xnx3_search_form')[0].getElementsByClassName('orderBy').length == 0){
							//还没有 orderBy 的input，增加
							// 创建一个输入框元素
							let input = document.createElement("input");
							// 设置输入框的类型为文本
							input.type = "hidden";
							// 设置输入框的名称
							input.name = "orderBy";
							input.value = orderByValue;
							// 将输入框添加到表单中
							document.getElementsByClassName('toubu_xnx3_search_form')[0].appendChild(input);
						}
						
						wm.list(1);
					}
				}
			});
			
			
		}
		
	}
	
});
</script>
</html>