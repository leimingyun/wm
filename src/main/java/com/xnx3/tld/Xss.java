package com.xnx3.tld;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.xnx3.StringUtil;

/**
 * Xss 处理
 * @author 管雷鸣
 */
public class Xss extends TagSupport {
	private String text;							//显示的字符串
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public int doEndTag() throws JspException {
		if(this.text != null){
			this.text = StringUtil.filterXss(text);
		}
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(this.text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return super.doEndTag();
	}
}
