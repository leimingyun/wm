package com.xnx3.j2ee.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;

import com.xnx3.Lang;

import cn.zvo.page.bean.TagA;

/**
 * 分页
 * @author 管雷鸣
 *
 */
public class Page extends cn.zvo.page.Page{
	private int limitStart;	//limit 的开始
	private String url;	//当前列表网址，后跟随除CURRENTPAGENAME外的其他所有参数
	private int allRecordNumber;	//总条数
	private int currentPageNumber;	//当前页面，当前第几页
	private int everyNumber;	//每页显示多少条
	private int lastPageNumber;	//最后一页是编号为几的页数
	private String nextPage;	//下一页URL
	private String upPage;		//上一页URL
	private String lastPage;	//尾页URL
	private String firstPage;	//首页，第一页URL
	private boolean haveNextPage;	//是否还有下一页
	private boolean haveUpPage;		//是否还有上一页
	private boolean currentLastPage;	//当前是否是最后一页
	private boolean currentFirstPage;	//当前是否是首页，第一页
	private int upPageNumber;	//上一页的页码
	private int nextPageNumber;	//下一页的页码
	private List<TagA> upList;	//向上的list分页标签
	private List<TagA> nextList;	//向下的分页list标签
	
	/*** 上面这些的作用是能使用javadoc自动生成文档 ***/

	/**
	 * @param allRecordNumber 共多少条
	 * @param everyNumber 每页多少条
	 * @param request HttpServletRequest
	 * 			get方式传入值：
	 * 			<ul>
	 * 				<li>currentPage：请求第几页。若不传，默认请求第一页</li>
	 * 				<li>orderBy：排序方式，如"user.id_DESC"，若不传则sql不会拼接上ORDER BY</li>
	 * 			</ul>
	 * 			<p>如果不传，则分页数据中的超链接相关不会显示<p>
	 */
	public Page(int allRecordNumber, int everyNumber, HttpServletRequest request) {
		this.setAllRecordNumber(allRecordNumber);
		this.setEveryNumber(everyNumber);
		
		if(request != null) {
			String url = request.getRequestURL().toString();
			String queryString = request.getQueryString();
			if(queryString != null && queryString.length() > 0) {
				url = url + "?" + queryString;
			}
			//ConsoleUtil.debug(url);
			this.setUrl(url);
			this.setCurrentPageNumber(Lang.stringToInt(request.getParameter("currentPage"), 1));
		}else {
			this.setCurrentPageNumber(1);
		}
	}
	
	/**
	 * 当前页默认赋予第一页
	 * @param allRecordNumber 共多少条
	 * @param everyNumber 每页多少条
	 */
	public Page(int allRecordNumber, int everyNumber) {
		this.setAllRecordNumber(allRecordNumber);
		this.setEveryNumber(everyNumber);
		this.setCurrentPageNumber(1);
	}
	
	/**
	 * 设置
	 * @param page
	 */
	public Page(cn.zvo.page.Page page, HttpServletRequest request) {
		if(page == null) {
			return;
		}
		BeanUtils.copyProperties(page, this);
		
		if(request != null) {
			String url = request.getRequestURL().toString();
			String queryString = request.getQueryString();
			if(queryString != null && queryString.length() > 0) {
				url = url + "?" + queryString;
			}
			//ConsoleUtil.debug(url);
			this.setUrl(url);
		}
	}
}
