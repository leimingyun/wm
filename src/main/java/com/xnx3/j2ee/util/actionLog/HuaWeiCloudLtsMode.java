package com.xnx3.j2ee.util.actionLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSON;
//import com.huawei.Client;
//import com.huawei.exception.ClientException;
//import com.huawei.model.LogResult;
//import com.huawei.model.client.ClientConfig;
//import com.huawei.model.pushLog.LogContent;
//import com.huawei.model.pushLog.LogItem;
//import com.huawei.model.pushLog.LogItems;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ApplicationPropertiesUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.vo.ActionLogListVO;

/**
 * 使用华为云LTS
 * application.properties 配置文件中定义 
 * 		wm.huaweicloud.lts.accessKey
 * 		wm.huaweicloud.lts.secretKey
 * 		wm.huaweicloud.lts.projectId
 * 		wm.huaweicloud.lts.region
 * 
 * 老秦实现方法实现优先级排序 - add、cacheCommit、list
 * @author 秦艺文
 *
 */
public class HuaWeiCloudLtsMode implements ActionLogInterface{
	public static final String indexName = "useraction";

	/**
	 * 全局
	 */
//	public static ClientConfig clientConfig = null;
	/**
	 * 日志
	 */
//	public static LogItems logItems = null;
	/**
	 * 日志组ID
	 */
	public static String groupid = null;
	/**
	 * 日志流ID
	 */
	public static String streamid = null;

	static{
		//初始化
		String accessKey = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.accessKey");
		String secretKey = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.secretKey");
		String projectId = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.projectId");
		String region = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.region");
		groupid = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.groupId");
		streamid = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.steamId");

		//判断是否使用
		if(accessKey != null && accessKey.length() > 0){
			//使用
			ConsoleUtil.info("开启 huaweicloud LTS 进行操作记录");

			/*
			 * 
			 * 自动检测用户动作的索引是否存在,不存在自动创建
			 * 进行一些初始化操作
			 * 
			 */

//			// 初始化链接配置
//			try {
//				clientConfig = ClientConfig.custom()
//						.setAccessKey(accessKey)	// 当前租户的AK（Access Key）
//						.setSecretKey(secretKey)	// 当前租户的SK（SecretKey Key）
//						.setProjectId(projectId)	// 当前租户的项目ID（Project Id）
//						.setRegion(region)			// 当前租户的区域信息
//						//.enableCompression(true)	// 发送日志是否需要压缩（当日志内容过大，请开启true。不设置此参数，默认为false不开启压缩）
//						.builder();
//				ConsoleUtil.info("初始化clientConfig完成: " + clientConfig.toString());
//				logItems = new LogItems();
//			} catch (ClientException e) {
//				ConsoleUtil.info("初始化clientConfig异常: " + e.toString());
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			//动作记录使用此
//			ActionLogUtil.actionLogInterface = new HuaWeiCloudLtsMode();
		}
	}

	@Override
	public void add(Map<String, Object> map) {
//		ConsoleUtil.info("进入add方法 ##### ");
//		if(clientConfig == null){
//			//不使用，终止即可
//			return;
//		}
//		ConsoleUtil.info("开始编写日志 ######");
//		// 日志列表
//		List<LogItem> logItemList = new ArrayList<>();
//
//		for (int i = 0; i < 1; i++) {
//			ConsoleUtil.info("进入循环, 创建LogItem对象 ####### ");
//			LogItem logItem = new LogItem();
//			logItem.setTenantProjectId(clientConfig.getProjectId());
//			ConsoleUtil.info("往日志对象中添加项目组ID: " + logItem.toString());
//			String lables = JSON.toJSONString(map);
//			logItem.setLabels(lables);
//			ConsoleUtil.info("往日志对象中添加日志内容map: " + logItem.toString());
//			List<LogContent> contents = new ArrayList<>();
//			// 将map的内容存到logContent
//			for(Map.Entry<String, Object> entry : map.entrySet()){
//				ConsoleUtil.info("进入日志内容循环体 #####");
//				String value = null;
//				if(entry.getValue() == null){
//					value = "";
//				}else{
//					value = entry.getValue().toString()+"";
//				}
//				ConsoleUtil.info("value值为: " + value);
//				
//				LogContent logContent = new LogContent();
//				logContent.setLogTimeNs(System.currentTimeMillis() * 1000000);
//				logContent.setLog(value);
//				ConsoleUtil.info("日志内容对象信息logContent: " + logContent.toString());
//				contents.add(logContent);
//				ConsoleUtil.info("日志内容集合信息contents: " + contents.toString());
//			}
//			logItem.setContents(contents);
//			ConsoleUtil.info("日志对象信息logItem: " + logItem.toString());
//			logItemList.add(logItem);
//			ConsoleUtil.info("日志集合信息logItemList: " + logItemList.toString());
//		}
//		logItems.setLogItems(logItemList);
//		
//		//推送提交
//		cacheCommit();
	}



	@Override
	public boolean cacheCommit() {
//		ConsoleUtil.info("进入推送接口cacheCommit ##### ");
//		if(clientConfig == null){
//			//不使用，终止即可
//			return false;
//		}
//		ConsoleUtil.info("日志集合信息logItemList: " + logItems.toString());
//		try {
//			// 创建链接
//			Client client = new Client(clientConfig);
//			ConsoleUtil.info("创建Client: " + client.toString());
//			// 发送日志（日志组ID，日志流ID，日志）
//			final LogResult result = client.pushLog(groupid,streamid, logItems);
//			ConsoleUtil.info("发送日志结果result: " + result.toString());
//			if (result.isSuccess()) {
//				ConsoleUtil.debug("push log success");
//				logItems.getLogItems().clear(); //推送成功，那么清空集合
//			} else {
//				ConsoleUtil.debug("push log failed" + JSON.toJSONString(result));
//			}
//			
//			return true;
//		} catch (Exception e) {
//			ConsoleUtil.info("发送日志结异常: " + e.toString());
//			e.printStackTrace();
//			return false;
//		}
		
		return false;
	}

	@Override
	public ActionLogListVO list(String query,int everyPageNumber, HttpServletRequest request) {
		return null;
	}

	/**
	 * 同上面的 list,只不过这里可以自定义操作 indexName。
	 * <p>获取到的数据排序规则：这里是按照数据加入的顺序，倒序排列，插入越往后的，显示越靠前</p>
	 * @param indexName 索引名字
	 * @param query 查询条件，传入如： name:guanleiming AND age:123
	 * @param everyPageNumber 每页显示几条，最大200
	 * @param request
	 * @return 如果失败， vo.getResult() == ActionLogListVO.FAILURE
	 */
	public ActionLogListVO list(String indexName, String query,int everyPageNumber, HttpServletRequest request) {

		return null;
	}
	
	public static void main(String[] args) throws InterruptedException {
		ConsoleUtil.debug = true;
		
		HuaWeiCloudLtsMode lts = new HuaWeiCloudLtsMode();
//		Thread.sleep(5000);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userid", "1");
		map.put("username", "admin");
		map.put("action", "运行main测试的");
		lts.add(map);
		
		boolean commit = lts.cacheCommit();
		System.out.println("提交结果："+commit);
	}
}
