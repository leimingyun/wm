package com.xnx3.j2ee.util.actionLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.vo.ActionLogListVO;
import com.xnx3.net.AliyunLogPageUtil;
import com.xnx3.net.AliyunLogUtil;
import net.sf.json.JSONArray;

/**
 * 日志记录，使用阿里云日志服务
 * @author 管雷鸣
 *
 */
public class AliyunSLSMode{
	public static AliyunLogUtil aliyunLogUtil = null;
	static String keyId;
	static String keySecret;
	static String endpoint;
	static String project;
	
	static{
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(Global.system.size() < 1){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
//				//当数据库加载完后，再初始化
//				//判断是否使用日志服务进行日志记录，条件便是 accessKeyId 是否为空。若为空，则不使用
//				String use = SystemUtil.get("ALIYUN_SLS_USE");
//				if(use != null && use.equals("1")){
//					//使用日志服务
//					
//					keyId = SystemUtil.get("ALIYUN_ACCESSKEYID");
//					keySecret = SystemUtil.get("ALIYUN_ACCESSKEYSECRET");
//					endpoint = SystemUtil.get("ALIYUN_SLS_ENDPOINT");
//					project = SystemUtil.get("ALIYUN_SLS_PROJECT");
//					String logstore = SystemUtil.get("ALIYUN_SLS_USERACTION_LOGSTORE");
//					
//					//最大超时时间
//					int log_cache_max_time = SystemUtil.getInt("ALIYUN_SLS_CACHE_MAX_TIME");
//					if(log_cache_max_time == 0){
//						log_cache_max_time = 120;
//					}
//					//最大条数
//					int log_cache_max_number = SystemUtil.getInt("ALIYUN_SLS_CACHE_MAX_NUMBER");
//					if(log_cache_max_number == 0){
//						log_cache_max_number = 100;
//					}
//					
//					
//					if(keyId.length() > 10){
//						aliyunLogUtil = new AliyunLogUtil(endpoint,  keyId, keySecret, project, logstore);
//						//开启触发日志的，其来源类及函数的记录
//						aliyunLogUtil.setStackTraceDeep(5);
//						aliyunLogUtil.setCacheAutoSubmit(log_cache_max_number, log_cache_max_time);
//						ConsoleUtil.info("开启阿里云日志服务进行操作记录");
//					}else{
//						//此处可能是还没执行install安装
//					}
//					
//					ActionLogUtil.actionLogInterface = new AliyunSLSMode();
//				}
				
				/*
				 *  ActionLogUtil.aliyunLogUtil 废弃,这里只是向上兼容而已
				 */
//				ActionLogUtil.aliyunLogUtil = AliyunSLSMode.aliyunLogUtil;
				
			}
		}).start();
		
		
	}

}
