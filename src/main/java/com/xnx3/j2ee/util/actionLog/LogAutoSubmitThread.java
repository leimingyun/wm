package com.xnx3.j2ee.util.actionLog;

import org.springframework.stereotype.Component;
import com.xnx3.j2ee.util.ActionLogUtil;

/**
 * 每间隔10秒钟自动提交一次日志
 * @author 管雷鸣
 *
 */
//@Component
public class LogAutoSubmitThread extends Thread{
	public LogAutoSubmitThread() {
		this.setName("ActionLogUtil Auto Submit Thread");
		this.start();
	}
	
	@Override
	public void run() {
		while(true){
			//每10秒钟提交一次
			try {
				sleep(10*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
//			if(ActionLogUtil.isUse()){
//				try {
//					ActionLogUtil.actionLogInterface.cacheCommit();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			
		}
	}
	
}
