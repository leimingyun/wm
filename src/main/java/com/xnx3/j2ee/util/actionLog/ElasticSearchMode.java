package com.xnx3.j2ee.util.actionLog;

//import com.xnx3.elasticsearch.ElasticSearchUtil;

/**
 * elasticsearch 模块。这里调试使用的是 7.10.1 版本
 * @author 管雷鸣
 *
 */
public class ElasticSearchMode{
	public static final String indexName = "useraction";
	
	/**
	 * @deprecated 废弃，使用 {@link com.xnx3.j2ee.util.ElasticSearchUtil#getElasticSearch()}
	 */
//	public static ElasticSearchUtil es;
	
//	static{
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				//初始化
//				String hostname = ApplicationPropertiesUtil.getProperty("wm.elasticsearch.hostname");
//				int port = Lang.stringToInt(ApplicationPropertiesUtil.getProperty("wm.elasticsearch.port"), 9200);
//				String scheme = ApplicationPropertiesUtil.getProperty("wm.elasticsearch.scheme");
//				String username = ApplicationPropertiesUtil.getProperty("wm.elasticsearch.username");
//				String password = ApplicationPropertiesUtil.getProperty("wm.elasticsearch.password");
//				
//				
//				//判断是否使用es进行日志记录，条件便是 hostname 是否为空。若为空，则不使用
//				if(hostname != null && hostname.length() > 0){
//					//使用
//					es = new ElasticSearchUtil(hostname, port, scheme);
//					if((username != null && username.length() > 0) && (password != null && password.length() > 0)) {
//						//使用账号密码模式
//						es.setUsernameAndPassword(username, password);
//					}
//					ConsoleUtil.info("开启ElasticSearch进行操作记录");
//					
//					//自动检测用户动作的索引是否存在
//					if(!es.existIndex("useraction")){
//						//如果不存在，那么创建
//						try {
//							CreateIndexResponse res = es.createIndex(indexName);
//							ConsoleUtil.info("检测到用户动作的存储索引不存在，已自动创建useraction索引。");
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
//					}
//					
//					//重写序列化接口
//					es.setJsonFormatInterface(new JsonFormatInterface() {
//						@Override
//						public String mapToJsonString(Map<String, Object> params) {
//							if(params == null){
//								params = new HashMap<String, Object>();
//							}
//							return JSONObject.fromObject(params).toString();
//						}
//					});
//					
//					//动作记录使用本es的
//					ActionLogUtil.actionLogInterface = new ElasticSearchMode();
//				}
//			}
//		}).start();
//	}

}
