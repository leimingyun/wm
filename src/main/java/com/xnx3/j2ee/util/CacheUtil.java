package com.xnx3.j2ee.util;

/**
 * 缓存工具。
 * 若使用Redis，在 application.properties 中配置了redis，那么这里是使用redis进行的缓存
 * 如果没有使用redis，那么这里使用的是 Hashmap 进行的缓存
 * @author 管雷鸣
 *
 */
public class CacheUtil extends com.xnx3.CacheUtil {
	
//	//关联 userid 跟当前用户登录之后的sessionid
	public final static String SHIRO_USERID = "shiro:userid:{userid}";
//	//session在redis中缓存的key，如果使用了redis的话
//	public final static String SHIRO_SESSION = "shiro:session:{sessionid}";
//	//用户登录成功后，有什么资源权限，比如可以查看网站、管理用户、查看日志等
//	public final static String SHIRO_CUSTOMREALM_AUTHORIZATION = "shiro:cache:com.xnx3.j2ee.shiro.CustomRealm.authorizationCache:{userid}";
//	
	
}
