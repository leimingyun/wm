package com.xnx3.j2ee.util.mq;

import com.xnx3.j2ee.util.ConsoleUtil;

/**
 * RabbitMQ topic 工具类
 * @author 管雷鸣
 *
 */
//public class RabbitUtil extends com.xnx3.rabbitmq.RabbitUtil{
public class RabbitUtil{

	/**
	 * 设置RabbitMQ相关信息
	 * @param host rabbitMQ所在的ip，如 100.51.15.10
	 * @param username rabbitMQ登陆的username，如rabbitMQ安装后默认的账户 guest
	 * @param password 登陆密码
	 * @param port 端口号，默认为 5672
	 */
	public RabbitUtil(String host, String username, String password, int port) {
		ConsoleUtil.debug("wm v3.0版本将默认的mq支持去掉了，需要的单独引入");
	}
}
