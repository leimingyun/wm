package com.xnx3.j2ee.util;

import com.xnx3.Lang;
import com.xnx3.j2ee.vo.BaseVO;

/**
 * 短信发送
 * @author 管雷鸣
 *
 */
public class SMSUtil {
	private static com.xnx3.SMSUtil smsUtil;
	private static int uid;
	private static String password;
	
	static {
		String uids = ApplicationPropertiesUtil.getProperty("sms.uid");
		uid = Lang.stringToInt(uids, 0);
		password = ApplicationPropertiesUtil.getProperty("sms.password");
		if(uid < 1) {
			ConsoleUtil.debug("sms.uid is not set!");
		}else {
			smsUtil = new com.xnx3.SMSUtil(uid, password);
		}
	}
	
	/**
	 * 发送一条短信。短信内容自己定，不过前缀会加上签名。
	 * @param phone 接收短信的手机号
	 * @param content 发送短信的内容。不加签名，比如这里传入“哈哈哈”,那么用户接收到的短信为 
	 * 		<pre>
	 * 			【雷鸣云】哈哈哈
	 * 		</pre>
	 * @return 其中 {@link BaseVO#getResult()} 为执行状态，是否成功
	 * 		<ul>
	 *	 		<li>{@link BaseVO#SUCCESS} 	：失败,可以通过 {@link BaseVO#getInfo()} 获得失败原因 </li>
	 * 			<li>{@link BaseVO#FAILURE} 	：成功,可以通过 {@link BaseVO#getInfo()} 获得发送的这个短信的消息唯一编号</li>
	 * 		</ul>
	 */
	public static BaseVO send(String phone, String content){
		com.xnx3.BaseVO sendVO = smsUtil.send(phone, content);
		if(sendVO.getResult() - BaseVO.FAILURE == 0) {
			return BaseVO.failure(sendVO.getInfo());
		}else {
			return BaseVO.success(sendVO.getInfo());
		}
	}
}
