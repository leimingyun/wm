package com.xnx3.j2ee.util.AttachmentMode;

import java.io.InputStream;
import java.util.List;
import com.xnx3.BaseVO;
import cn.zvo.fileupload.bean.SubFileBean;
import cn.zvo.fileupload.framework.springboot.FileUploadUtil;

/**
 * 附件上传之 阿里云 OSS
 * 已废弃，使用 https://github.com/xnx3/FileUpload
 * @author 管雷鸣
 * @deprecated 
 */
public class AliyunOSSMode{

	public long getDirectorySize(String path) {
		return FileUploadUtil.getDirectorySize(path);
	}

	public void copyFile(String originalFilePath, String newFilePath) {
		FileUploadUtil.copy(originalFilePath, newFilePath);
	}

	public List<SubFileBean> getSubFileList(String path) {
		return FileUploadUtil.getSubFileList(path);
	}

	public long getFileSize(String path) {
		return FileUploadUtil.getFileSize(path);
	}

	public cn.zvo.fileupload.vo.UploadFileVO uploadFile(String path, InputStream inputStream) {
		return FileUploadUtil.upload(path, inputStream);
	}

	public InputStream getFile(String path) {
//		return FileUploadUtil.getInputStream(path);
		System.out.println("过时，请用 fileupload");
		return null;
	}

	public BaseVO deleteFile(String filePath) {
		FileUploadUtil.delete(filePath);
		return BaseVO.success();
	}

	public BaseVO createFolder(String path) {
		FileUploadUtil.createFolder(path);
		return BaseVO.success();
	}
}
