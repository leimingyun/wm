package com.xnx3.j2ee.util.AttachmentMode;

import cn.zvo.fileupload.storage.local.LocalStorage;

/**
 * 附件上传之 服务器本身存储，服务器本地存储，附件存储到服务器硬盘上
 * @author 管雷鸣
 * @deprecated
 */
public class LocalServerMode extends LocalStorage{
	
	/**
	 * 目录检测，检测是否存在。若不存在，则自动创建目录。适用于使用本地磁盘进行存储，在本身tomcat中创建目录.有一下两种情况:
	 * <ul>
	 * 		<li>在线上的tomcat项目中，创建的目录是在 tomcat/webapps/ROOT/ 目录下</li>
	 * 		<li>在开发环境Eclipse中，创建的目录是在 target/classes/ 目录下</li>
	 * </ul>
	 * @param path 要检测的目录，相对路径，如 jar/file/  创建到file文件，末尾一定加/     或者jar/file/a.jar创建到file文件夹
	 */
	public static void directoryInitCreate(String path){
		LocalStorage storage = new LocalStorage();
		storage.directoryInit(path);
	}
}
