package com.xnx3.j2ee.util.AttachmentMode.bean;

/**
 * 子文件信息
 * @author 管雷鸣
 *
 */
public class SubFileBean extends cn.zvo.fileupload.bean.SubFileBean{
	
	public SubFileBean() {
		size = 0;
		folder = false;
	}
	
}
