package com.xnx3.j2ee.util.AttachmentMode;

import java.io.InputStream;
import java.util.List;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.AttachmentMode.bean.SubFileBean;
import com.xnx3.j2ee.util.AttachmentMode.hander.OBSHandler;
import com.xnx3.j2ee.vo.UploadFileVO;

/**
 * feiqi
 * @author 管雷鸣
 * @deprecated
 */
public class HuaweiyunOBSMode {
	
	public static OBSHandler obsHandler;	//禁用，通过getObsUtil() 获取
	public static String obsBucketName;		// 当前进行操作桶的名称
	
	public static OBSHandler getObsHander() {
		ConsoleUtil.error("com.xnx3.j2ee.util.AttachmentMode.HuaweiyunOBSMode 已废弃，请使用 https://gitee.com/mail_osc/FileUpload");
		return obsHandler;
	}
	
	public void putStringFile(String path, String text, String encode) {
		
	}
	
	public UploadFileVO put(String path, InputStream inputStream) {
		return null;
	}
	
	public String getTextByPath(String path) {
		return null;
	}

	public void deleteObject(String filePath) {
	}
	public long getDirectorySize(String path) {
		return 0;
	}
	
	public void copyObject(String originalFilePath, String newFilePath) {
	}
	public List<SubFileBean> getSubFileList(String path) {
		return null;
	}

	public long getFileSize(String path) {
		return 0;
	}

	public void createFolder(String path) {
	}
	
}
