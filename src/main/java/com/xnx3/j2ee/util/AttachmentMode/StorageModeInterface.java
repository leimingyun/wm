package com.xnx3.j2ee.util.AttachmentMode;


/**
 * 存储模块接口。比如阿里云、华为云、服务器本地存储，都要实现这个接口
 * <br/>已过时，请使用 cn.zvo.file.StorageModeInterface
 * @author 管雷鸣
 * @deprecated
 */
public interface StorageModeInterface extends cn.zvo.fileupload.StorageInterface {

}