package com.xnx3.j2ee.util;

import org.springframework.scheduling.annotation.Scheduled;

import com.xnx3.DateUtil;

/**
 * Scheduled 的管理，配合 Thread 一起使用
 * @author Administrator
 *
 */
public class ScheduledUtil extends Thread{
	public boolean isrun = false; //当前已经运行了，避免这个线程被重复运行
	public int lasttime = DateUtil.timeForUnix10(); //最后一次执行时间，10位时间戳，如果运行异常，isrun没有变过来，也会根据这个时间，如果超过5小时还没变过来，那么证明线程异常没有触发isrun=false，那么这里强制进行改变状态
	public int expireTime = 60*60*5; //过时时间，默认是5小时，距离上一次线程执行超过5小时，则认为线程出现异常了，强制执行
	
	
	/**
	 * 守护进程，这里判断如果isrun超过5小时没变那就是出现了异常，强制将 isrun设置位false
	 */
	public void guard() {
		if(DateUtil.timeForUnix10() > lasttime + expireTime) {
			isrun = false;
		}
	}
	
	/**
	 * 更新最后时间
	 */
	public void updateLasttime() {
		lasttime = DateUtil.timeForUnix10();
	}
	
	/**
	 * 获取当前是否允许运行
	 * true 允许向下运行
	 * false 不允许运行，还没达到运行条件
	 * @return
	 */
	public boolean isAllowRun() {
		guard();
		
		if(isrun) {
			return false;
		}
		return true;
	}
	
	
	
	
}
