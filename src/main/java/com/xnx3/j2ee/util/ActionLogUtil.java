package com.xnx3.j2ee.util;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import com.xnx3.BaseVO;
import com.xnx3.DateUtil;
import com.xnx3.Lang;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.shiro.ShiroFunc;
import com.xnx3.j2ee.vo.ActionLogListVO;
import com.xnx3.net.AliyunLogUtil;
import com.xnx3.net.OSSUtil;
import cn.zvo.fileupload.framework.springboot.FileUploadUtil;
import cn.zvo.log.datasource.console.ConsoleDataSource;
import cn.zvo.log.framework.springboot.ApplicationConfig;
import cn.zvo.log.framework.springboot.LogUtil;
import cn.zvo.log.vo.LogListVO;

/**
 * 
 * 会员动作日志的缓存及使用。
 * 有其他日志需要记录，可以参考这个类。可吧这个类复制出来，在此基础上进行修改
 * @author 管雷鸣
 */
@Component
public class ActionLogUtil {
	
	/**
	 * 日志类型：普通日志
	 * <br/>比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。
	 * <br/>默认不传入此参数时，也是使用此种方式
	 */
	public static final String TYPE_NORMAL = "NORMAL";
	/**
	 * 日志类型：错误日志
	 * <br/>记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。
	 * <br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录
	 */
	public static final String TYPE_ERROR = "ERROR";
	/**
	 * 日志类型：数据库有变动的。
	 * <br/>凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此种类型。
	 */
	public static final String TYPE_UPDATE_DATABASE = "UPDATE_DATABASE";
	
	/**
	 * 已被 actionLogInterface 替代。
	 * @deprecated
	 */
	public static AliyunLogUtil aliyunLogUtil = null;
	static{
		//这里直接初始化两个mode，看那个有值就先用那个
//		new ElasticSearchMode();
//		new AliyunSLSMode();
//		//v2.14增加，判断是否使用华为云的日志，华为云只能在华为云服务器才能用这个日志
//		String huaweiAccessKey = ApplicationPropertiesUtil.getProperty("wm.huaweicloud.lts.accessKey");
//		if(huaweiAccessKey != null && huaweiAccessKey.length() > 0) {
//			ConsoleUtil.info("使用华为云日志服务，注意华为云日志服务要手动加入三个jar包。详见wm中引入的三个本地jar，不然启动时会报错");
//			new HuaWeiCloudLtsMode();
//		}
//		
		
	}
	
	public ActionLogUtil() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(Global.system.size() < 1){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				//当数据库加载完后，再初始化
				
				
				
				//判断是否使用日志服务进行日志记录，条件便是 accessKeyId 是否为空。若为空，则不使用
				String use = SystemUtil.get("ALIYUN_SLS_USE");
				if(use != null && use.equals("1")){
					//使用日志服务
					
					String keyId = SystemUtil.get("ALIYUN_ACCESSKEYID");
					String keySecret = SystemUtil.get("ALIYUN_ACCESSKEYSECRET");
					String endpoint = SystemUtil.get("ALIYUN_SLS_ENDPOINT");
					String project = SystemUtil.get("ALIYUN_SLS_PROJECT");
					String logstore = SystemUtil.get("ALIYUN_SLS_USERACTION_LOGSTORE");
					
					//最大超时时间
					int log_cache_max_time = SystemUtil.getInt("ALIYUN_SLS_CACHE_MAX_TIME");
					if(log_cache_max_time == 0){
						log_cache_max_time = 120;
					}
					//最大条数
					int log_cache_max_number = SystemUtil.getInt("ALIYUN_SLS_CACHE_MAX_NUMBER");
					if(log_cache_max_number == 0){
						log_cache_max_number = 100;
					}
					
					
//					if(keyId.length() > 10){
//						aliyunLogUtil = new AliyunLogUtil(endpoint,  keyId, keySecret, project, logstore);
//						//开启触发日志的，其来源类及函数的记录
//						aliyunLogUtil.setStackTraceDeep(5);
//						aliyunLogUtil.setCacheAutoSubmit(log_cache_max_number, log_cache_max_time);
//						ConsoleUtil.info("开启阿里云日志服务进行操作记录");
//					}else{
//						//此处可能是还没执行install安装
//					}
					
					ConsoleUtil.info("aliyun sls init start...");
					
					Map<String, Map<String, String>> datasource = new HashMap<String, Map<String,String>>();
					Map<String, String> aliyunSLS = new HashMap<String, String>();
					aliyunSLS.put("accessKeyId", keyId);
					aliyunSLS.put("accessKeySecret", keySecret);
					aliyunSLS.put("endpoint", endpoint);
					aliyunSLS.put("project", project);
					datasource.put("aliyunSLS", aliyunSLS);
					
					ApplicationConfig config = new ApplicationConfig();
					config.setDataSource(datasource);
					config.setCacheMaxTime(log_cache_max_time+"");
					config.setCacheMaxNumber(log_cache_max_number+"");
					ConsoleUtil.debug(config.toString());
					LogUtil.log.loadConfig(config);
					LogUtil.log.setTable(logstore);
					ConsoleUtil.debug("log -- 使用 aliyunSLS , interface : "+LogUtil.getLog().getDatasource().getClass().getName());
					
					System.out.println("=== 提醒，后续再新项目中配置日志时，请在application.properties中配置。详细参见：  ===");
					System.out.println("参见 https://github.com/xnx3/log");
					System.out.println("===========");
//					ActionLogUtil.actionLogInterface = new AliyunSLSMode();
				}
				
				
			}
		}).start();
		
	}
	
	/**
	 * 实现日志服务记录的接口
	 */
//	public static ActionLogInterface actionLogInterface = null;
	
	/**
	 * 是否使用此动作日志
	 * 因为默认就是控制台输出方式，所以此方法已无效
	 * @return true:使用
	 * @deprecated
	 */
	public static boolean isUse(){
		if(LogUtil.isDataSource(ConsoleDataSource.class)) {
			//默认得控制台输出
			return false;
		}
		return true;
	}
	
	
	/**
	 * 插入一条日志。
	 * <b>注意，不能直接使用此方法此写日志</b>，是因为上面设置了 aliyunLogUtil.setStackTraceDeep(4); 如果直接使用此方法写日志，那么执行的类、方法 是记录不到的。这个方法是给 insert....方法 提供服务的
	 * <p>已废弃，请使用 {@link #logExtend(Map, HttpServletRequest, Integer, String, String, String)} </p>
	 * @param logItem 传入要保存的logItem，若为空，则会创建一个新的。此项主要为扩展使用，可自行增加其他信息记录入日志
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @param type 当前日志记录的类型。 取值：
	 * 			<ul>
	 * 				<li> {@link #TYPE_NORMAL} : 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。 </li>
	 * 				<li> {@link #TYPE_ERROR} : 错误日志。记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。<br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录<br/>这种类型一般不会用到。除非自己感觉哪里可能会有坑，留一条这种类型的日志</li>
	 * 				<li> {@link #TYPE_UPDATE_DATABASE} : 操作数据库相关的。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此种类型<br/>使用 insertUpdateDatabase(...) 这种方法名的，就是记录这种类型的日志 </li>
	 * 			</ul>
	 * @deprecated
	 */
//	public static synchronized void logExtend(LogItem logItem, HttpServletRequest request, Integer goalid, String action, String remark, String type){
//		if(!isUse()){
//			//不使用日志，终止即可
//			return;
//		}
//		
//		Map<String, Object> params = AliyunSLSMode.logitemToMap(logItem);
//		logExtend(params, request, goalid, action, remark, type);
//	}

	/**
	 * 插入一条日志。
	 * <b>注意，不能直接使用此方法此写日志</b>，是因为上面设置了 aliyunLogUtil.setStackTraceDeep(4); 如果直接使用此方法写日志，那么执行的类、方法 是记录不到的。这个方法是给 insert....方法 提供服务的
	 * @param params 传入要保存的键值对。
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @param type 当前日志记录的类型。 取值：
	 * 			<ul>
	 * 				<li> {@link #TYPE_NORMAL} : 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。 </li>
	 * 				<li> {@link #TYPE_ERROR} : 错误日志。记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。<br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录<br/>这种类型一般不会用到。除非自己感觉哪里可能会有坑，留一条这种类型的日志</li>
	 * 				<li> {@link #TYPE_UPDATE_DATABASE} : 操作数据库相关的。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此种类型<br/>使用 insertUpdateDatabase(...) 这种方法名的，就是记录这种类型的日志 </li>
	 * 			</ul>
	 */
	public static synchronized void logExtend(Map<String, Object> params, HttpServletRequest request, Integer goalid, String action, String remark, String type){
		if(params == null){
			params = new HashMap<String, Object>();
		}
		
		/*用户相关信息,只有用户登录后，才会记录用户信息*/
		User user = ShiroFunc.getUser();
		if(user != null){
			params.put("userid", user.getId());
			params.put("username", user.getUsername());
		}
		
		/* 动作相关 */
		if(goalid != null){
			params.put("goalid", goalid);
		}
		if(action != null){
			params.put("action", action);
		}
		if(remark != null){
			params.put("remark", remark);
		}
		
		/*浏览器自动获取的一些信息*/
		if(request != null){
			params.put("ip", IpUtil.getIpAddress(request));
			params.put("param", request.getQueryString());
			params.put("url", request.getRequestURL().toString());
			params.put("referer", request.getHeader("referer"));
			params.put("userAgent", request.getHeader("User-Agent"));
			params.put("path", request.getServletPath());
		}
		
		//类型
		if(type == null || type.length() == 0){
			type = TYPE_NORMAL;
		}
		params.put("type", type);
		params.put("time", DateUtil.timeForUnix13());
		
		logExtend(params);
	}

	/**
	 * 插入一条日志。这是最原始的插入，不会附带当前登录用户、来源、以及在那个方法执行等。除非在mq接收消息等一些特殊场景、J2SE等项目的场景，才会用这个，不然，还是用 insert....这个开头的方法吧
	 * <p>此方法已废弃，请使用 {@link #logExtend(Map)} </p>
	 * @param logItem 传入要保存的 {@link LogItem}
	 * @deprecated
	 */
//	public static synchronized void logExtend(LogItem logItem){
//		if(!isUse()){
//			return;
//		}
//		
//		actionLogInterface.add(AliyunSLSMode.logitemToMap(logItem));
//	}
	
	/**
	 * 插入一条日志。这是最原始的插入，不会附带当前登录用户、来源、以及在那个方法执行等。
	 * 除非你是自定义数据类型，才会用这个，不然，还是用 insert....这个开头的方法吧
	 * @param params 传入要保存的键值对
	 */
	public static synchronized void logExtend(Map<String, Object> params){
//		if(!isUse()){
//			return;
//		}
//		
		if(params == null){
			return;
		}
		if(params.size() == 0){
			return;
		}
		
//		actionLogInterface.add(params);
		LogUtil.add(params);
	}
	
	/**
	 * 插入一条日志。
	 * <p>已废弃，请使用 {@link #insert(Map, HttpServletRequest, Integer, String, String, String)} </p>
	 * @param logItem 传入要保存的logItem，若为空，则会创建一个新的。此项主要为扩展使用，可自行增加其他信息记录入日志
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @param type 当前日志记录的类型。 取值：
	 * 			<ul>
	 * 				<li> {@link #TYPE_NORMAL} : 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。 </li>
	 * 				<li> {@link #TYPE_ERROR} : 错误日志。记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。<br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录<br/>这种类型一般不会用到。除非自己感觉哪里可能会有坑，留一条这种类型的日志</li>
	 * 				<li> {@link #TYPE_UPDATE_DATABASE} : 操作数据库相关的。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此种类型<br/>使用 insertUpdateDatabase(...) 这种方法名的，就是记录这种类型的日志 </li>
	 * 			</ul>
	 * @deprecated
	 */
//	public static synchronized void insert(LogItem logItem, HttpServletRequest request, Integer goalid, String action, String remark, String type){
//		logExtend(AliyunSLSMode.logitemToMap(logItem), request, goalid, action, remark, type);
//	}
	
	/**
	 * 插入一条日志。
	 * @param params 传入要保存的键值对
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @param type 当前日志记录的类型。 取值：
	 * 			<ul>
	 * 				<li> {@link #TYPE_NORMAL} : 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。 </li>
	 * 				<li> {@link #TYPE_ERROR} : 错误日志。记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。<br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录<br/>这种类型一般不会用到。除非自己感觉哪里可能会有坑，留一条这种类型的日志</li>
	 * 				<li> {@link #TYPE_UPDATE_DATABASE} : 操作数据库相关的。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此种类型<br/>使用 insertUpdateDatabase(...) 这种方法名的，就是记录这种类型的日志 </li>
	 * 			</ul>
	 */
	public static synchronized void insert(Map<String, Object> params, HttpServletRequest request, Integer goalid, String action, String remark, String type){
		logExtend(params, request, goalid, action, remark, type);
	}
	
	/**
	 * 插入一条日志。
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * <p>已废弃，请使用 {@link #insert(Map, HttpServletRequest, Integer, String, String)} </p>
	 * @param logItem 传入要保存的logItem，若为空，则会创建一个新的。此项主要为扩展使用，可自行增加其他信息记录入日志
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @deprecated
	 */
//	public static synchronized void insert(LogItem logItem, HttpServletRequest request, Integer goalid, String action, String remark){
//		logExtend(AliyunSLSMode.logitemToMap(logItem), request, goalid, action, remark, TYPE_NORMAL);
//	}
	
	/**
	 * 插入一条日志。
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * @param params 传入要保存的键值对
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static synchronized void insert(Map<String, Object> params, HttpServletRequest request, Integer goalid, String action, String remark){
		logExtend(params, request, goalid, action, remark, TYPE_NORMAL);
	}
	
	/**
	 * 插入一条日志
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * 
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static synchronized void insert(HttpServletRequest request, Integer goalid, String action, String remark){
		logExtend(new HashMap<String, Object>(), request, goalid, action, remark, TYPE_NORMAL);
	}
	
	/**
	 * 插入一条日志。
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static void insert(HttpServletRequest request, String action, String remark){
		logExtend(new HashMap<String, Object>(), request, null, action, remark, TYPE_NORMAL);
	}
	
	/**
	 * 插入一条日志
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 */
	public static void insert(HttpServletRequest request, Integer goalid, String action){
		logExtend(new HashMap<String, Object>(), request, goalid, action, null, TYPE_NORMAL);
	}
	
	/**
	 * 插入一条日志
	 * <br/>这里插入的日志类型是 {@link #TYPE_NORMAL} 正常类型，比如用户进入某个页面、查看什么详情、查看什么列表等，只是记录用户普通的动作。如果是对数据库有更改、新增、删除操作的，需要使用 insertUpdateDatabase(...)
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param action 动作的名字，如：用户登录、更改密码
	 */
	public static void insert(HttpServletRequest request, String action){
		logExtend(new HashMap<String, Object>(), request, null, action, null, TYPE_NORMAL);
	}
	
	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * <p>已废弃，请使用 {@link #insertUpdateDatabase(Map, HttpServletRequest, Integer, String, String)} </p>
	 * @param logItem 传入要保存的logItem，若为空，则会创建一个新的。此项主要为扩展使用，可自行增加其他信息记录入日志
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 * @deprecated
	 */
//	public static synchronized void insertUpdateDatabase(LogItem logItem, HttpServletRequest request, Integer goalid, String action, String remark){
//		logExtend(AliyunSLSMode.logitemToMap(logItem), request, goalid, action, remark, TYPE_UPDATE_DATABASE);
//	}
	
	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * @param params 传入要保存的键值对
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static synchronized void insertUpdateDatabase(Map<String, Object> params, HttpServletRequest request, Integer goalid, String action, String remark){
		logExtend(params, request, goalid, action, remark, TYPE_UPDATE_DATABASE);
	}

	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static synchronized void insertUpdateDatabase(HttpServletRequest request, Integer goalid, String action, String remark){
		logExtend(new HashMap<String, Object>(), request, goalid, action, remark, TYPE_UPDATE_DATABASE);
	}

	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param action 动作的名字，如：用户登录、更改密码
	 * @param remark 动作的描述，如用户将名字张三改为李四
	 */
	public static void insertUpdateDatabase(HttpServletRequest request, String action, String remark){
		logExtend(new HashMap<String, Object>(), request, null, action, remark, TYPE_UPDATE_DATABASE);
	}
	
	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param goalid 操作的目标的id，若无，可为0，也可为空
	 * @param action 动作的名字，如：用户登录、更改密码
	 */
	public static void insertUpdateDatabase(HttpServletRequest request, Integer goalid, String action){
		logExtend(new HashMap<String, Object>(), request, goalid, action, "", TYPE_UPDATE_DATABASE);
	}
	
	/**
	 * 插入一条数据库变动日志。凡是数据库有插入、修改、删除记录的，让数据库数据有变动的，都使用此方法记录日志。
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param action 动作的名字，如：用户登录、更改密码
	 */
	public static void insertUpdateDatabase(HttpServletRequest request, String action){
		logExtend(new HashMap<String, Object>(), request, null, action, "", TYPE_UPDATE_DATABASE);
	}
	
	/**
	 * 插入一条错误日志。
	 * <br/>记录理论上不会出现的错误，但实际用户使用时，真的出现了。出现这种记录，技术人员看到这种类型记录后，一定是程序中数据、逻辑出现问题了，需要排查的，这种记录的日志不能看完就忽略，一定是要经过技术排查。
	 * <br/>比如有一个订单，根据订单中的用户编号(userid)取用户表(User)的记录时，用户表中没有这个人，那这个就是程序在哪个地方出现问题了，就要技术人员进行排查了。这种信息就可以用 TYPE_ERROR 来进行记录
	 * @param request HttpServletRequest 若为空，则不日志中不记录请求的信息，比如浏览器信息、请求网址等
	 * @param remark 详细描述，如： 有一个订单，订单号是xxx,根据订单中的用户编号userid:xxxx取用户表(User)的记录时，用户表中没有这个人，
	 */
	public static void insertError(HttpServletRequest request, String remark){
		logExtend(new HashMap<String, Object>(), request, null, "ERROR LOG", remark, TYPE_ERROR);
	}
	
	/**
	 * 将缓存中的日志提交
	 * @return true:成功
	 */
	public static boolean cacheCommit(){
		return LogUtil.commit();
	}
	
	/**
	 * 将日志查询出来，以列表+分页的数据输出
	 * @param query 要查询的单词或者文字、又或者传入如： name:guanleiming AND age:123
	 * @param everyPageNumber 每页显示多少条，也就是list返回多少条数据
	 * @param request {@link HttpServletRequest} 分页会使用到这个
	 * @return {@link ActionLogListVO}
	 */
	public static ActionLogListVO list(String query, int everyPageNumber, HttpServletRequest request){
		int currentPage = 1;
		if(request.getParameter("currentPage") != null) {
			currentPage = Lang.stringToInt(request.getParameter("currentPage"), 1);
		}
		LogListVO resultList = LogUtil.list(query, everyPageNumber, currentPage);
		
		ActionLogListVO vo = new ActionLogListVO();
		if(resultList.getResult() - LogListVO.FAILURE == 0) {
			vo.setResult(resultList.getResult());
			vo.setInfo(resultList.getInfo());
		}else {
			vo.setJsonArray(resultList.getJsonArray());
			vo.setResult(resultList.getResult());
			vo.setInfo(resultList.getInfo());
			resultList.getPage().setUrl(query);
			vo.setPage(new Page(resultList.getPage(), request));
		}
		
		return vo;
	}
	
	/**
	 * 同上面的 list,只不过这里可以自定义操作 indexName。【已无效】
	 * <p>获取到的数据排序规则：这里是按照数据加入的顺序，倒序排列，插入越往后的，显示越靠前</p>
	 * @param indexName 索引名字
	 * @param query 查询条件，传入如： name:guanleiming AND age:123
	 * @param everyPageNumber 每页显示几条，最大100
	 * @param request {@link HttpServletRequest} 分页会使用到这个
	 * @return 如果失败， vo.getResult() == ActionLogListVO.FAILURE
	 * @deprecated
	 */
	public static ActionLogListVO list(String indexName, String query,int everyPageNumber, HttpServletRequest request){
//		return actionLogInterface.list(indexName, query, everyPageNumber, request);
		ActionLogListVO vo = new ActionLogListVO();
		vo.setBaseVO(BaseVO.FAILURE,"接口废弃");
		return vo;
	}
	
}
