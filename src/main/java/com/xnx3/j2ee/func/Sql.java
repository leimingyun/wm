package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.SpringUtil;

/**
 * Java JDBC 操作Mysql， 连接是从spring中取的
 * @author 管雷鸣
 * @deprecated 以废弃，请使用 {@link SpringUtil}
 */
public class Sql extends SpringUtil{
	
}
