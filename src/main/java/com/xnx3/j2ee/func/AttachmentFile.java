package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.AttachmentUtil;

/**
 * 文件上传，附件的操作，如OSS、或服务器本地文件
 * 如果是localFile ，则需要设置 AttachmentFile.netUrl
 * @author 管雷鸣
 * @deprecated 已废弃！请使用 {@link AttachmentUtil}
 */
public class AttachmentFile extends AttachmentUtil{
	
}
