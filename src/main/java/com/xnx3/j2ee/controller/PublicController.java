package com.xnx3.j2ee.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.util.AttachmentUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileRuleVO;

/**
 * 通用的一些
 * @author 管雷鸣
 */
@Controller(value="WMPublicController")
@RequestMapping("/")
public class PublicController extends BaseController {
	
	/**
	 * 403
	 */
	@RequestMapping("403${url.suffix}")
	public String error403(HttpServletRequest request){
		return "wm/403";
	}
	
	/**
	 * 404
	 */
	@RequestMapping("404${url.suffix}")
	public String error404(HttpServletRequest request){
		return "wm/404";
	}
	

	/**
	 * 406，这里用406错误代码，来表示文件上传太大的返回http响应
	 */
	@RequestMapping("406${url.suffix}")
	@ResponseBody
	public BaseVO error406(HttpServletRequest request){
		return error("请上传大小在 "+ AttachmentUtil.getMaxFileSize()+" 之内的文件");
	}
	
	/**
	 * 500
	 */
	@RequestMapping("500${url.suffix}")
	public String error500(HttpServletRequest request){
		return "wm/500";
	}
	
	/**
	 * 文件上传，获取可上传文件后缀及最大允许上传文件的大小
	 * @author 管雷鸣
	 */
	@RequestMapping(value="uploadFileRule.json", method= {RequestMethod.POST})
	@ResponseBody
	public UploadFileRuleVO uploadImage(HttpServletRequest request){
		UploadFileRuleVO vo = new UploadFileRuleVO();
		vo.setAllowUploadSuffix(Global.ossFileUploadImageSuffixList);
		vo.setMaxFileSizeKB(AttachmentUtil.getMaxFileSizeKB());
		return vo;
	}
	
}
