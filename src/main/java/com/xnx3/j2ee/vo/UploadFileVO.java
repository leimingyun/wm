package com.xnx3.j2ee.vo;


/**
 * 文件上传相关
 * @author 管雷鸣
 */
public class UploadFileVO extends cn.zvo.fileupload.vo.UploadFileVO {
	//适配wm 2.25 以前版本
	private String fileName;
	
	public UploadFileVO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 上传成功后的返回值
	 * @param fileName 上传成功后的文件名，如 "xnx3.jar"
	 * @param path 上传成功后的路径，如 "/jar/file/xnx3.jar"
	 * @param url 文件上传成功后，外网访问的url
	 */
	public UploadFileVO(String fileName,String path,String url) {
		this.name = fileName;
		this.path = path;
		this.url = url;
	}

	public String getFileName() {
		return name;
	}

	public void setFileName(String fileName) {
		this.name = fileName;
	}
}
