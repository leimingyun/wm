package com.xnx3.j2ee.vo;

import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.system.responseBody.ResponseBodyManage;

/**
 * 登陆接口的返回值
 * @author 管雷鸣
 */
@ResponseBodyManage(ignoreField = {"password","salt","version"}, nullSetDefaultValue = true)
public class LoginVO extends BaseVO {
	
	private String token;	//sessionid
	private User user;		//登录的用户信息

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
