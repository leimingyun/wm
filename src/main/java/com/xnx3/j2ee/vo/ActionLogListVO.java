package com.xnx3.j2ee.vo;

import com.xnx3.j2ee.util.Page;

import net.sf.json.JSONArray;

/**
 * com.xnx3.j2ee.util.actionLog.list 方法返回所用
 * @author apple
 *
 */
public class ActionLogListVO extends BaseVO {
	
	private JSONArray jsonArray;	//json数组，数组内有json对象，每个json对象都是一列数据。数据都存在这里。
	private Page page;
	
	public JSONArray getJsonArray() {
		return jsonArray;
	}
	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	
}
