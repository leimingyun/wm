package com.xnx3.j2ee.vo;

import java.util.List;
import java.util.Map;

import com.xnx3.j2ee.util.Page;

/**
 * com.xnx3.j2ee.util.ElasticSearchUtil.list 方法返回所用
 * @author 管雷鸣
 */
public class ElasticSearchPageListVO extends BaseVO {
	
	private List<Map<String, Object>> list;	//json数组，数组内有json对象，每个json对象都是一列数据。数据都存在这里。
	private Page page;
	
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	
	@Override
	public String toString() {
		return "ElasticSearchPageListVO [list=" + list + ", page=" + page + "]";
	}
	
}
