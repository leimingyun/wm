package com.xnx3.j2ee.vo;

import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.system.responseBody.ResponseBodyManage;

/**
 * 用户
 * @author 管雷鸣
 */
@ResponseBodyManage(ignoreField = {"password","salt","version"}, nullSetDefaultValue = true)
public class UserVO extends BaseVO {
	
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
